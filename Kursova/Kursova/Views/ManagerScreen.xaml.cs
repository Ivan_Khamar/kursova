﻿using Kursova.Models;
using System;
using System.Data.Entity;
using System.Windows;
using System.Windows.Controls;

namespace Kursova.Views
{
    /// <summary>
    /// Interaction logic for ManagerScreen.xaml
    /// </summary>
    public partial class ManagerScreen : Window
    {
        KursovaEntities kursovaData;
        public ManagerScreen()
        {
            InitializeComponent();
            kursovaData = new KursovaEntities();
            kursovaData.Order.Load();
            dataGrid.ItemsSource = kursovaData.Order.Local.ToBindingList();
            kursovaData.Contract.Load();
            dataGrid_Contracts.ItemsSource = kursovaData.Contract.Local.ToBindingList();

            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            kursovaData.Dispose();
        }

        private void showOrdersDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.Visibility == Visibility.Hidden)
                {
                    dataGrid.Visibility = Visibility.Visible;
                    dataGrid.ItemsSource = kursovaData.Order.Local.ToBindingList();
                }
                else
                {
                    dataGrid.Visibility = Visibility.Hidden;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void addOrderBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBox.Show("Specify products needed.");

                MainWindow mainWindow = new MainWindow(this);
                mainWindow.Show();

                int statusRez = 0;

                string statusRezString = statusIDTextBox.Text.Trim();
                if (statusRezString != null)
                {
                    switch (statusRezString)
                    {
                        case "new":
                            statusRez = 1;
                            break;
                        case "assembling":
                            statusRez = 2;
                            break;
                        case "delivering":
                            statusRez = 3;
                            break;
                        case "done":
                            statusRez = 4;
                            break;
                        default:
                            MessageBox.Show("Order`s state can be new/assembling/delivering/done only.");
                            break;
                    }
                }
                else
                {
                    MessageBox.Show("Enter order`s state.");
                }

                Order order = new Order();
                order.order_date = DateTime.Now;
                order.order_description = orderDescriptionTextBox.Text.Trim();
                order.Worker_ID = UserInfo.worker_id;
                order.Contract_ID = Int32.Parse(contractIDTextBox.Text.Trim());
                if (statusRez == 1 || statusRez == 2 || statusRez == 3 || statusRez == 4)
                {
                    switch (statusRez)
                    {
                        case 1:
                            order.Status_ID = 1;
                            break;
                        case 2:
                            order.Status_ID = 2;
                            break;
                        case 3:
                            order.Status_ID = 3;
                            break;
                        case 4:
                            order.Status_ID = 4;
                            break;
                        default:
                            MessageBox.Show("Order`s state can be new/assembling/delivering/done only.");
                            break;
                    }
                }
                else
                {
                    MessageBox.Show("Enter valid order`s state.");
                }

                kursovaData.Order.Add(order);
                kursovaData.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void updateOrderButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int statusRez = 0;

                if (dataGrid.SelectedItem != null)
                {
                    Order updatedOrder = (Order)dataGrid.SelectedItem;
                    updatedOrder.order_date = DateTime.Now;
                    updatedOrder.order_description = orderDescriptionTextBox.Text.Trim();
                    updatedOrder.Worker_ID = UserInfo.worker_id;
                    updatedOrder.Contract_ID = Int32.Parse(contractIDTextBox.Text.Trim());
                    string statusRezString = statusIDTextBox.Text.Trim();
                    if (statusRezString != null)
                    {
                        switch (statusRezString)
                        {
                            case "new":
                                statusRez = 1;
                                break;
                            case "assembling":
                                statusRez = 2;
                                break;
                            case "delivering":
                                statusRez = 3;
                                break;
                            case "done":
                                statusRez = 4;
                                break;
                            default:
                                MessageBox.Show("Order`s state can be new/assembling/delivering/done only.");
                                break;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Enter order`s state.");
                    }
                    if (statusRez == 1 || statusRez == 2 || statusRez == 3 || statusRez == 4)
                    {
                        switch (statusRez)
                        {
                            case 1:
                                updatedOrder.Status_ID = 1;
                                break;
                            case 2:
                                updatedOrder.Status_ID = 2;
                                break;
                            case 3:
                                updatedOrder.Status_ID = 3;
                                break;
                            case 4:
                                updatedOrder.Status_ID = 4;
                                break;
                            default:
                                MessageBox.Show("Order`s state can be new/assembling/delivering/done only.");
                                break;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Enter valid order`s state.");
                    }

                    dataGrid.Items.Refresh();

                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to update.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void deleteOrderButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Order order = dataGrid.SelectedItem as Order;
                if (order != null)
                {
                    kursovaData.Order.Remove(order);
                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to delete.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void showContractsDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid_Contracts.Visibility == Visibility.Hidden)
                {
                    dataGrid_Contracts.Visibility = Visibility.Visible;
                    dataGrid_Contracts.ItemsSource = kursovaData.Contract.Local.ToBindingList();
                }
                else
                {
                    dataGrid_Contracts.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void logOutBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserInfo.worker_id = -1;
                UserInfo.worker_name = "";
                UserInfo.worker_email = "";
                UserInfo.worker_position = "";

                LoginScreen loginScreen = new LoginScreen();
                loginScreen.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void dataGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            Models.Order row_selected = dg.SelectedItem as Models.Order;
            if (row_selected != null)
            {
                orderDescriptionTextBox.Text = row_selected.order_description;
                contractIDTextBox.Text = row_selected.Contract_ID.ToString();
                switch (row_selected.Status_ID)
                {
                    case 1:
                        statusIDTextBox.Text = "new";
                        break;
                    case 2:
                        statusIDTextBox.Text = "assembling";
                        break;
                    case 3:
                        statusIDTextBox.Text = "delivering";
                        break;
                    case 4:
                        statusIDTextBox.Text = "done";
                        break;
                    default:
                        MessageBox.Show("Specify status for this order.");
                        break;
                }
            }
        }
    }
}
