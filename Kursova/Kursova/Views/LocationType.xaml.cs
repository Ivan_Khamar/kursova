﻿using Kursova.Models;
using System;
using System.Data.Entity;
using System.Windows;
using System.Windows.Controls;

namespace Kursova.Views
{
    /// <summary>
    /// Interaction logic for LocationType.xaml
    /// </summary>
    public partial class LocationType : Window
    {
        KursovaEntities kursovaData;
        DirectorScreen director;
        public LocationType()
        {
            InitializeComponent();
            kursovaData = new KursovaEntities();

            kursovaData.Location_types.Load();
            dataGrid.ItemsSource = kursovaData.Location_types.Local.ToBindingList();

            this.Closing += MainWindow_Closing;
        }

        public LocationType(DirectorScreen dir)
        {
            InitializeComponent();

            this.director = dir;
            dir.IsEnabled = false;

            kursovaData = new KursovaEntities();

            kursovaData.Location_types.Load();
            dataGrid.ItemsSource = kursovaData.Location_types.Local.ToBindingList();

            this.Closing += MainWindowParam_Closing;
        }

        private void MainWindowParam_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.director.IsEnabled = true;
            kursovaData.Dispose();
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            kursovaData.Dispose();
        }

        private void showLocationTypesDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.Visibility == Visibility.Hidden)
                {
                    dataGrid.Visibility = Visibility.Visible;
                    dataGrid.ItemsSource = kursovaData.Location_types.Local.ToBindingList();
                }
                else
                {
                    dataGrid.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void addTypeBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Location_types locType = new Location_types();
                locType.type_name = typeNameTextBox.Text.Trim();
                locType.type_description = typeDescriptionTextBox.Text.Trim();

                kursovaData.Location_types.Add(locType);
                kursovaData.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void updateTypeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.SelectedItem != null)
                {
                    Location_types updatedLocationType = (Location_types)dataGrid.SelectedItem;
                    updatedLocationType.type_name = typeNameTextBox.Text.Trim();
                    updatedLocationType.type_description = typeDescriptionTextBox.Text.Trim();

                    dataGrid.Items.Refresh();

                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to update.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void deleteTypeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Location_types locType = dataGrid.SelectedItem as Location_types;
                if (locType != null)
                {
                    kursovaData.Location_types.Remove(locType);
                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to delete.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void dataGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            Models.Location_types row_selected = dg.SelectedItem as Models.Location_types;
            if (row_selected != null)
            {
                typeNameTextBox.Text = row_selected.type_name;
                typeDescriptionTextBox.Text = row_selected.type_description;
            }
        }
    }
}
