﻿using Kursova.Models;
using System;
using System.Data.Entity;
using System.Windows;
using System.Windows.Controls;

namespace Kursova.Views
{
    /// <summary>
    /// Interaction logic for AdministratorScreen.xaml
    /// </summary>
    public partial class AdministratorScreen : Window
    {
        KursovaEntities kursovaData;
        public AdministratorScreen()
        {
            InitializeComponent();
            kursovaData = new KursovaEntities();

            kursovaData.Deliveries.Load();
            dataGrid.ItemsSource = kursovaData.Deliveries.Local.ToBindingList();

            kursovaData.Delivery_Details.Load();
            dataGrid_Details.ItemsSource = kursovaData.Delivery_Details.Local.ToBindingList();

            kursovaData.Location.Load();
            dataGrid_Locations.ItemsSource = kursovaData.Location.Local.ToBindingList();

            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            kursovaData.Dispose();
        }

        private void showProductsDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.Visibility == Visibility.Hidden)
                {
                    dataGrid.Visibility = Visibility.Visible;
                    dataGrid.ItemsSource = kursovaData.Deliveries.Local.ToBindingList();
                }
                else
                {
                    dataGrid.Visibility = Visibility.Hidden;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void addProductBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBox.Show("Specify delivery details.");

                Details details = new Details(this);
                details.Show();

                Deliveries delivery = new Deliveries();
                delivery.Creation_Date = DateTime.UtcNow; 
                delivery.Amount = Int32.Parse(deliveryAmountTextBox.Text.Trim());
                delivery.Worker_ID = UserInfo.worker_id;
                delivery.Location_ID = Int32.Parse(locationIDTextBox.Text.Trim());
                delivery.Date_recieved = null;
                delivery.Delivery_price = Int32.Parse(deliveryPriceTextBox.Text.Trim()); 
                delivery.Delivery_Details_ID = Int32.Parse(detailsIDTextBox.Text.Trim());

                kursovaData.Deliveries.Add(delivery);
                kursovaData.SaveChanges();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void updateProductButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.SelectedItem != null)
                {
                    Deliveries updatedDelivery = (Deliveries)dataGrid.SelectedItem;
                    updatedDelivery.Creation_Date = DateTime.UtcNow;
                    updatedDelivery.Amount = Int32.Parse(deliveryAmountTextBox.Text.Trim());
                    updatedDelivery.Worker_ID = UserInfo.worker_id;
                    updatedDelivery.Location_ID = Int32.Parse(locationIDTextBox.Text.Trim());
                    updatedDelivery.Date_recieved = null;
                    updatedDelivery.Delivery_price = Int32.Parse(deliveryPriceTextBox.Text.Trim());
                    updatedDelivery.Delivery_Details_ID = Int32.Parse(detailsIDTextBox.Text.Trim());

                    kursovaData.SaveChanges();

                    dataGrid.Items.Refresh();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to update.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void deleteProductButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Deliveries delivery = dataGrid.SelectedItem as Deliveries;
                if (delivery != null)
                {
                    kursovaData.Deliveries.Remove(delivery);
                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to delete.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void showCategoriesDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid_Details.Visibility == Visibility.Hidden)
                {
                    dataGrid_Details.Visibility = Visibility.Visible;
                    dataGrid_Details.ItemsSource = kursovaData.Delivery_Details.Local.ToBindingList();
                }
                else
                {
                    dataGrid_Details.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void logOutBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserInfo.worker_id = -1;
                UserInfo.worker_name = "";
                UserInfo.worker_email = "";
                UserInfo.worker_position = "";

                LoginScreen loginScreen = new LoginScreen();
                loginScreen.Show();
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void showLocationsDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid_Locations.Visibility == Visibility.Hidden)
                {
                    dataGrid_Locations.Visibility = Visibility.Visible;
                    dataGrid_Locations.ItemsSource = kursovaData.Location.Local.ToBindingList();
                }
                else
                {
                    dataGrid_Locations.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void dataGrid_SelectionChanged_1(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            Deliveries row_selected = dg.SelectedItem as Deliveries;
            if (row_selected != null)
            {
                deliveryAmountTextBox.Text = row_selected.Amount.ToString();
                deliveryPriceTextBox.Text = row_selected.Delivery_price.ToString();
                detailsIDTextBox.Text = row_selected.Delivery_Details_ID.ToString();
                locationIDTextBox.Text = row_selected.Location_ID.ToString() ;
            }
        }
    }
}
