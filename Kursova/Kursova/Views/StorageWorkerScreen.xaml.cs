﻿using Kursova.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Kursova.Views
{
    /// <summary>
    /// Interaction logic for StorageWorkerScreen.xaml
    /// </summary>
    public partial class StorageWorkerScreen : Window
    {
        KursovaEntities kursovaData;
        public StorageWorkerScreen()
        {
            InitializeComponent();
            kursovaData = new KursovaEntities();

            kursovaData.Products.Load();
            dataGrid.ItemsSource = kursovaData.Products.Local.ToBindingList();

            kursovaData.Categories.Load();
            dataGrid_Categories.ItemsSource = kursovaData.Categories.Local.ToBindingList();

            kursovaData.Deliveries.Load();
            dataGrid_Copy.ItemsSource = kursovaData.Deliveries.Local.ToBindingList();

            this.Closing += MainWindow_Closing;
        }
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            kursovaData.Dispose();
        }


        private void showProductsDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.Visibility == Visibility.Hidden)
                {
                    dataGrid.Visibility = Visibility.Visible;
                    dataGrid.ItemsSource = kursovaData.Products.Local.ToBindingList();
                }
                else
                {
                    dataGrid.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void addProductBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Products product = new Products();
                product.Product_Characteristics = productCharacteristicsTextBox.Text.Trim();
                product.Product_Description = productDescriptionTextBox.Text.Trim();
                product.Product_Price = Decimal.Parse(productPriceTextBox.Text.Trim());
                product.Product_Name = productNameTextBox.Text.Trim();
                product.Category_ID = Int32.Parse(categoryIDTextBox.Text.Trim());

                kursovaData.Products.Add(product);
                kursovaData.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void updateProductButton_Click(object sender, RoutedEventArgs e)
        {
            try
            { 
                if (dataGrid.SelectedItem != null)
                {
                    Products updatedProduct = (Products)dataGrid.SelectedItem;
                    updatedProduct.Product_Characteristics = productCharacteristicsTextBox.Text.Trim();
                    updatedProduct.Product_Description = productDescriptionTextBox.Text.Trim();
                    updatedProduct.Product_Price = Decimal.Parse(productPriceTextBox.Text.Trim());
                    updatedProduct.Product_Name = productNameTextBox.Text.Trim();  
                    updatedProduct.Category_ID = Int32.Parse(categoryIDTextBox.Text.Trim());
                
                    dataGrid.Items.Refresh();

                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to update.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
           
        }

        private void deleteProductButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Products product = dataGrid.SelectedItem as Products;
                if (product != null)
                {
                    kursovaData.Products.Remove(product);
                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to delete.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void showCategoriesDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid_Categories.Visibility == Visibility.Hidden)
                {
                    dataGrid_Categories.Visibility = Visibility.Visible;
                    dataGrid_Categories.ItemsSource = kursovaData.Categories.Local.ToBindingList();
                }
                else
                {
                    dataGrid_Categories.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void logOutBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserInfo.worker_id = -1;
                UserInfo.worker_name = "";
                UserInfo.worker_email = "";
                UserInfo.worker_position = "";

                LoginScreen loginScreen = new LoginScreen();
                loginScreen.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void updateOrder(Deliveries updatedDelivery) 
        {
            try
            {
                List<Kursova.Models.Delivery_Details> delDetails = new List<Kursova.Models.Delivery_Details>();
                delDetails = kursovaData.Delivery_Details.ToList();

                List<Kursova.Models.Order_Products> orderProducts = new List<Kursova.Models.Order_Products>();
                orderProducts = kursovaData.Order_Products.ToList();

                List<Kursova.Models.Order> orders = new List<Kursova.Models.Order>();
                orders = kursovaData.Order.ToList();

                try
                {
                    for (int i = 0; i < delDetails.Count - 1; i++)
                    {
                        if (delDetails[i].Delivery_Details_ID == updatedDelivery.Delivery_Details_ID)
                        {
                            for (int j = 0; j < orderProducts.Count - 1; i++)
                            {
                                if (orderProducts[j].Order_Product_ID == delDetails[i].Order_Product_ID)
                                {
                                    for (int k = 0; k < orders.Count - 1; i++)
                                    {
                                        if (orders[k].Order_ID == orderProducts[j].Order_ID)
                                        {
                                            orders[k].Status_ID = 4;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.GetType().ToString());
                }

                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }

           
        }

        private void setRecieveDateBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                dataGrid_Copy.Items.Refresh();
                if (dataGrid_Copy.SelectedItem != null)
                {
                    Deliveries updatedDelivery = (Deliveries)dataGrid_Copy.SelectedItem;
                    Deliveries tempDelivery = updatedDelivery;
                    updatedDelivery.Date_recieved = DateTime.UtcNow;

                    updatedDelivery.Creation_Date = tempDelivery.Creation_Date;
                    updatedDelivery.Amount = tempDelivery.Amount;
                    updatedDelivery.Worker_ID = UserInfo.worker_id;
                    updatedDelivery.Location_ID = tempDelivery.Location_ID;
                    updatedDelivery.Delivery_price = tempDelivery.Delivery_price;
                    updatedDelivery.Delivery_Details_ID = tempDelivery.Delivery_Details_ID;

                    kursovaData.SaveChanges();

                    dataGrid_Copy.Items.Refresh();

                    dataGrid_Copy.ItemsSource = kursovaData.Deliveries.Local.ToBindingList();

                    dataGrid_Copy.Items.Refresh();

                    //////////////////////////////////////////////////////////////////////////////

                    updateOrder(updatedDelivery);
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to recieve.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void recieveDelieveriesBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                dataGrid_Copy.Items.Refresh();
                if (dataGrid_Copy.Visibility == Visibility.Hidden)
                {
                    setRecieveDateBtn.Visibility = Visibility.Visible;
                    dataGrid_Copy.Visibility = Visibility.Visible;
                    dataGrid_Copy.ItemsSource = kursovaData.Deliveries.Local.ToBindingList();
                }
                else
                {
                    setRecieveDateBtn.Visibility = Visibility.Hidden;
                    dataGrid_Copy.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void dataGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            Models.Products row_selected = dg.SelectedItem as Models.Products;
            if (row_selected != null)
            {
                productNameTextBox.Text = row_selected.Product_Name;
                productDescriptionTextBox.Text = row_selected.Product_Description;
                productCharacteristicsTextBox.Text = row_selected.Product_Characteristics;
                productPriceTextBox.Text = row_selected.Product_Price.ToString();
                categoryIDTextBox.Text = row_selected.Category_ID.ToString();
            }
        }
    }
}
