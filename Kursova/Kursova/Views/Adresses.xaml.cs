﻿using Kursova.Models;
using System;
using System.Data.Entity;
using System.Windows;
using System.Windows.Controls;

namespace Kursova.Views
{
    /// <summary>
    /// Interaction logic for Adresses.xaml
    /// </summary>
    public partial class Adresses : Window
    {
        KursovaEntities kursovaData;
        DirectorScreen director;
        public Adresses()
        {
            InitializeComponent();
            kursovaData = new KursovaEntities();
            kursovaData.Address.Load();
            dataGrid.ItemsSource = kursovaData.Address.Local.ToBindingList();
            this.Closing += MainWindow_Closing;
        }

        public Adresses(DirectorScreen dir)
        {
            InitializeComponent();

            this.director = dir;
            dir.IsEnabled = false;

            kursovaData = new KursovaEntities();
            kursovaData.Address.Load();
            dataGrid.ItemsSource = kursovaData.Address.Local.ToBindingList();
            this.Closing += MainWindowParam_Closing;
        }

        private void MainWindowParam_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.director.IsEnabled = true;
            kursovaData.Dispose();
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            kursovaData.Dispose();
        }

        private void showAdressDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.Visibility == Visibility.Hidden)
                {
                    dataGrid.Visibility = Visibility.Visible;
                    dataGrid.ItemsSource = kursovaData.Address.Local.ToBindingList();
                }
                else
                {
                    dataGrid.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void addAdressBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Address position = new Address();
                position.Adress_Street = addressStreetTextBox.Text.Trim();
                position.Adress_Building = Int32.Parse(addressBuildingTextBox.Text.Trim());
                position.Adress_City = addressCityTextBox.Text.Trim();

                kursovaData.Address.Add(position);
                kursovaData.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void updateAdressButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.SelectedItem != null)
                {
                    Address updatedPosition = (Address)dataGrid.SelectedItem;
                    updatedPosition.Adress_Street = addressStreetTextBox.Text.Trim();
                    updatedPosition.Adress_Building = Int32.Parse(addressBuildingTextBox.Text.Trim());
                    updatedPosition.Adress_City = addressCityTextBox.Text.Trim();

                    dataGrid.Items.Refresh();

                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to update.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void deleteAdressButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Address adres = dataGrid.SelectedItem as Address;
                if (adres != null)
                {
                    kursovaData.Address.Remove(adres);
                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to delete.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void dataGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            Models.Address row_selected = dg.SelectedItem as Models.Address;
            if (row_selected != null)
            {
                addressCityTextBox.Text = row_selected.Adress_City;
                addressStreetTextBox.Text = row_selected.Adress_Street;
                addressBuildingTextBox.Text = row_selected.Adress_Building.ToString();
            }
        }
    }
}
