﻿using Kursova.Models;
using System;
using System.Data.Entity;
using System.Windows;
using System.Windows.Controls;

namespace Kursova.Views
{
    /// <summary>
    /// Interaction logic for Positions.xaml
    /// </summary>
    public partial class Positions : Window
    {
        KursovaEntities kursovaData;
        DirectorScreen director;
        public Positions()
        {
            InitializeComponent();
            kursovaData = new KursovaEntities();
            kursovaData.Position.Load();
            dataGrid_Positions.ItemsSource = kursovaData.Position.Local.ToBindingList();
            this.Closing += MainWindow_Closing;
        }

        public Positions(DirectorScreen dir)
        {
            InitializeComponent();

            this.director = dir;
            dir.IsEnabled = false;

            kursovaData = new KursovaEntities();
            kursovaData.Position.Load();
            dataGrid_Positions.ItemsSource = kursovaData.Position.Local.ToBindingList();
            this.Closing += MainWindowParam_Closing;
        }

        private void MainWindowParam_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.director.IsEnabled = true;
            kursovaData.Dispose();
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            kursovaData.Dispose();
        }

        private void showOrdersDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid_Positions.Visibility == Visibility.Hidden)
                {
                    dataGrid_Positions.Visibility = Visibility.Visible;
                    dataGrid_Positions.ItemsSource = kursovaData.Position.Local.ToBindingList();
                }
                else
                {
                    dataGrid_Positions.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void addOrderBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Position position = new Position();
                position.Position_name = positionNameTextBox.Text.Trim();
                position.Position_salary = Decimal.Parse(positionSalaryTextBox.Text.Trim());
                position.Position_requirements = positionRequirementsTextBox.Text.Trim();
                position.Position_responsibilities = positionResponsibilitiesTextBox.Text.Trim();

                kursovaData.Position.Add(position);
                kursovaData.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void updateOrderButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid_Positions.SelectedItem != null)
                {
                    Position updatedPosition = (Position)dataGrid_Positions.SelectedItem;
                    updatedPosition.Position_name = positionNameTextBox.Text.Trim();
                    updatedPosition.Position_salary = Decimal.Parse(positionSalaryTextBox.Text.Trim());
                    updatedPosition.Position_requirements = positionRequirementsTextBox.Text.Trim();
                    updatedPosition.Position_responsibilities = positionResponsibilitiesTextBox.Text.Trim();

                    dataGrid_Positions.Items.Refresh();

                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to update.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void deleteOrderButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Position position = dataGrid_Positions.SelectedItem as Position;
                if (position != null)
                {
                    kursovaData.Position.Remove(position);
                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to delete.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void logOutBtn_Click(object sender, RoutedEventArgs e)
        {
            /*try
            {
                UserInfo.worker_id = -1;
                UserInfo.worker_name = "";
                UserInfo.worker_email = "";
                UserInfo.worker_position = "";

                LoginScreen loginScreen = new LoginScreen();
                loginScreen.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }*/
            
        }
        private void showContractsDataBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void dataGrid_Positions_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            Models.Position row_selected = dg.SelectedItem as Models.Position;
            if (row_selected != null)
            {
                positionNameTextBox.Text = row_selected.Position_name;
                positionSalaryTextBox.Text = row_selected.Position_salary.ToString();
                positionRequirementsTextBox.Text = row_selected.Position_requirements;
                positionResponsibilitiesTextBox.Text = row_selected.Position_responsibilities;
            }
        }
    }
}
