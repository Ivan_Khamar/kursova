﻿using Kursova.Models;
using System;
using System.Data.Entity;
using System.Windows;
using System.Windows.Controls;

namespace Kursova.Views
{
    /// <summary>
    /// Interaction logic for Workers.xaml
    /// </summary>
    public partial class Workers : Window
    {
        KursovaEntities kursovaData;
        DirectorScreen director;
        public Workers()
        {
            InitializeComponent();

            kursovaData = new KursovaEntities();
            kursovaData.Workers.Load();
            dataGrid_Workers.ItemsSource = kursovaData.Workers.Local.ToBindingList();

            this.Closing += MainWindow_Closing;
        }

        public Workers(DirectorScreen dir)
        {
            InitializeComponent();

            this.director = dir;
            dir.IsEnabled = false;

            kursovaData = new KursovaEntities();
            kursovaData.Workers.Load();
            dataGrid_Workers.ItemsSource = kursovaData.Workers.Local.ToBindingList();

            this.Closing += MainWindowParam_Closing;
        }

        private void MainWindowParam_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.director.IsEnabled = true;
            kursovaData.Dispose();
        }
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            kursovaData.Dispose();
        }
        private void showWorkersDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid_Workers.Visibility == Visibility.Hidden)
                {
                    dataGrid_Workers.Visibility = Visibility.Visible;
                    dataGrid.ItemsSource = kursovaData.Workers.Local.ToBindingList();
                }
                else
                {
                    dataGrid_Workers.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }
        private void addWorkerBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Kursova.Models.Workers worker = new Kursova.Models.Workers();
                worker.worker_name = workerNameTextBox.Text.Trim();
                worker.worker_phone = workerPhoneTextBox.Text.Trim();
                worker.worker_email = workerEmailTextBox.Text.Trim();
                worker.worker_pass = workerPasswordTextBox.Text.Trim();
                worker.other_details = otherDetailsTextBox.Text.Trim();

                if (positionIDTextBox.Text.Trim() == "Manager")
                {
                    worker.Position_ID = 1;
                }
                else
                {
                    if (positionIDTextBox.Text.Trim() == "Storage worker")
                    {
                        worker.Position_ID = 3;
                    }
                    else
                    {
                        if (positionIDTextBox.Text.Trim() == "Administrator")
                        {
                            worker.Position_ID = 4;
                        }
                        else
                        {
                            if (positionIDTextBox.Text.Trim() == "Accountant")
                            {
                                worker.Position_ID = 5;
                            }
                            else
                            {
                                if (positionIDTextBox.Text.Trim() == "Director")
                                {
                                    worker.Position_ID = 6;
                                }
                                else
                                {
                                    worker.Position_ID = 0;
                                    MessageBox.Show("Enter existing position: Manager/Storage worker/Administrator/Accountant/Director.");
                                }
                            }
                        }
                    }
                }

                //worker.Position_ID = Int32.Parse(positionIDTextBox.Text.Trim());
                if (worker.Position_ID != 0)
                {
                    kursovaData.Workers.Add(worker);
                    kursovaData.SaveChanges();
                }
                else
                {
                    MessageBox.Show("Invalid data. Worker wasn`t added.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void updateWorkerButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid_Workers.SelectedItem != null)
                {
                    Kursova.Models.Workers updatedWorker = (Kursova.Models.Workers)dataGrid_Workers.SelectedItem;
                    updatedWorker.worker_name = workerNameTextBox.Text.Trim();
                    updatedWorker.worker_phone = workerPhoneTextBox.Text.Trim();
                    updatedWorker.worker_email = workerEmailTextBox.Text.Trim();
                    updatedWorker.worker_pass = workerPasswordTextBox.Text.Trim();
                    updatedWorker.other_details = otherDetailsTextBox.Text.Trim();

                    if (positionIDTextBox.Text.Trim() == "Manager")
                    {
                        updatedWorker.Position_ID = 1;
                    }
                    else
                    {
                        if (positionIDTextBox.Text.Trim() == "Storage worker")
                        {
                            updatedWorker.Position_ID = 3;
                        }
                        else
                        {
                            if (positionIDTextBox.Text.Trim() == "Administrator")
                            {
                                updatedWorker.Position_ID = 4;
                            }
                            else
                            {
                                if (positionIDTextBox.Text.Trim() == "Accountant")
                                {
                                    updatedWorker.Position_ID = 5;
                                }
                                else
                                {
                                    if (positionIDTextBox.Text.Trim() == "Director")
                                    {
                                        updatedWorker.Position_ID = 6;
                                    }
                                    else
                                    {
                                        updatedWorker.Position_ID = 0;
                                        MessageBox.Show("Enter existing position: Manager/Storage worker/Administrator/Accountant/Director.");
                                    }
                                }
                            }
                        }
                    }

                    //updatedWorker.Position_ID = Int32.Parse(positionIDTextBox.Text.Trim());

                    if (updatedWorker.Position_ID != 0)
                    {
                        dataGrid_Workers.Items.Refresh();

                        kursovaData.SaveChanges();
                    }
                    else
                    {
                        MessageBox.Show("Invalid data. Worker wasn`t updated.");
                    }
                    
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to update.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void deleteWorkerButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Kursova.Models.Workers worker = dataGrid_Workers.SelectedItem as Kursova.Models.Workers;
                if (worker != null)
                {
                    kursovaData.Workers.Remove(worker);
                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to delete.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void logOutBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserInfo.worker_id = -1;
                UserInfo.worker_name = "";
                UserInfo.worker_email = "";
                UserInfo.worker_position = "";

                LoginScreen loginScreen = new LoginScreen();
                loginScreen.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }
        private void showContractsDataBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void dataGrid_Workers_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            Models.Workers row_selected = dg.SelectedItem as Models.Workers;
            if (row_selected != null)
            {
                workerNameTextBox.Text = row_selected.worker_name;
                workerPhoneTextBox.Text = row_selected.worker_phone;
                workerEmailTextBox.Text = row_selected.worker_email;
                otherDetailsTextBox.Text = row_selected.other_details;

                if (row_selected.Position_ID == 1)
                {
                    positionIDTextBox.Text = "Manager";
                }
                else
                {
                    if (row_selected.Position_ID == 3)
                    {
                        positionIDTextBox.Text = "Storage worker";
                    }
                    else
                    {
                        if (row_selected.Position_ID == 4)
                        {
                            positionIDTextBox.Text = "Administrator";
                        }
                        else
                        {
                            if (row_selected.Position_ID == 5)
                            {
                                positionIDTextBox.Text = "Accountant";
                            }
                            else
                            {
                                if (row_selected.Position_ID == 6)
                                {
                                    positionIDTextBox.Text = "Director";
                                }
                                else
                                {
                                    MessageBox.Show("Enter existing position: Manager/Storage worker/Administrator/Accountant/Director.");
                                }
                            }
                        }
                    }
                }

                //positionIDTextBox.Text = row_selected.Position_ID.ToString();
                workerPasswordTextBox.Text = "---";
            }
        }
    }
}
