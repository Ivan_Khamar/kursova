﻿using Kursova.Models;
using System;
using System.Data.Entity;
using System.Windows;
using System.Windows.Controls;

namespace Kursova.Views
{
    /// <summary>
    /// Interaction logic for Locations.xaml
    /// </summary>
    public partial class Locations : Window
    {
        KursovaEntities kursovaData;
        DirectorScreen director;
        public Locations()
        {
            InitializeComponent();
            kursovaData = new KursovaEntities();

            kursovaData.Location.Load();
            dataGrid.ItemsSource = kursovaData.Location.Local.ToBindingList();

            kursovaData.Location_types.Load();
            dataGrid_LocationType.ItemsSource = kursovaData.Location_types.Local.ToBindingList();

            kursovaData.Address.Load();
            dataGrid_Adress.ItemsSource = kursovaData.Address.Local.ToBindingList();

            this.Closing += MainWindow_Closing;
        }

        public Locations(DirectorScreen dir)
        {
            InitializeComponent();

            this.director = dir;
            dir.IsEnabled = false;

            kursovaData = new KursovaEntities();

            kursovaData.Location.Load();
            dataGrid.ItemsSource = kursovaData.Location.Local.ToBindingList();

            kursovaData.Location_types.Load();
            dataGrid_LocationType.ItemsSource = kursovaData.Location_types.Local.ToBindingList();

            kursovaData.Address.Load();
            dataGrid_Adress.ItemsSource = kursovaData.Address.Local.ToBindingList();

            this.Closing += MainWindowParam_Closing;
        }
        private void MainWindowParam_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.director.IsEnabled = true;
            kursovaData.Dispose();
        }
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            kursovaData.Dispose();
        }

        private void showLocationsDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.Visibility == Visibility.Hidden)
                {
                    dataGrid.Visibility = Visibility.Visible;
                    dataGrid.ItemsSource = kursovaData.Location.Local.ToBindingList();
                }
                else
                {
                    dataGrid.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void addLocationBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Location location = new Location();
                location.Location_name = locationNameTextBox.Text.Trim();
                location.Location_type_ID = Int32.Parse(locationTypeTextBox.Text.Trim());
                location.Adress_ID = Int32.Parse(adressIDTextBox.Text.Trim());

                kursovaData.Location.Add(location);
                kursovaData.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void updateLocationButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.SelectedItem != null)
                {
                    Location updatedLocation = (Location)dataGrid.SelectedItem;
                    updatedLocation.Location_name = locationNameTextBox.Text.Trim();
                    updatedLocation.Location_type_ID = Int32.Parse(locationTypeTextBox.Text.Trim());
                    updatedLocation.Adress_ID = Int32.Parse(adressIDTextBox.Text.Trim());

                    dataGrid.Items.Refresh();

                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to update.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void deleteLocationButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Location location = dataGrid.SelectedItem as Location;
                if (location != null)
                {
                    kursovaData.Location.Remove(location);
                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to delete.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void showAdressesBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid_Adress.Visibility == Visibility.Hidden)
                {
                    dataGrid_Adress.Visibility = Visibility.Visible;
                    dataGrid_Adress.ItemsSource = kursovaData.Address.Local.ToBindingList();
                }
                else
                {
                    dataGrid_Adress.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void showLocationTypesBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid_LocationType.Visibility == Visibility.Hidden)
                {
                    dataGrid_LocationType.Visibility = Visibility.Visible;
                    dataGrid_LocationType.ItemsSource = kursovaData.Location_types.Local.ToBindingList();
                }
                else
                {
                    dataGrid_LocationType.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void logOutBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void dataGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            Models.Location row_selected = dg.SelectedItem as Models.Location;
            if (row_selected != null)
            {
                locationNameTextBox.Text = row_selected.Location_name;
                locationTypeTextBox.Text = row_selected.Location_type_ID.ToString();
                adressIDTextBox.Text = row_selected.Adress_ID.ToString();
            }
        }
    }
}
