﻿using Kursova.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Windows;

namespace Kursova.Views
{
    /// <summary>
    /// Interaction logic for AccountantScreen.xaml
    /// </summary>
    public partial class AccountantScreen : Window
    {
        KursovaEntities kursovaData;
        public AccountantScreen()
        {
            InitializeComponent();
            kursovaData = new KursovaEntities();

            kursovaData.Deliveries.Load();
            dataGrid.ItemsSource = kursovaData.Deliveries.Local.ToBindingList();

            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            kursovaData.Dispose();
        }

        private void showProductsDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                decimal sum1 = 0;
            
                for (int i = 0; i < dataGrid.Items.Count-1; i++)
                {
                    Deliveries updatedDelivery = (Deliveries)dataGrid.Items[i];
                    sum1 += updatedDelivery.Delivery_price;
                }

                label_Copy5.Content = sum1.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void addProductBtn_Click(object sender, RoutedEventArgs e)
        {
            List<Kursova.Models.Workers> workers = new List<Kursova.Models.Workers>();
            workers = kursovaData.Workers.ToList();
            decimal salarySum = new decimal();
            for (int i = 0; i < workers.Count; i++)
            {
                //MessageBox.Show(workers[i].Position_ID.ToString());
                switch (workers[i].Position_ID)
                {
                    case 1:
                        salarySum += 6300;
                        break;
                    case 3:
                        salarySum += 4000;
                        break;
                    case 4:
                        salarySum += 7400;
                        break;
                    case 5:
                        salarySum += 6900;
                        break;
                    case 6:
                        salarySum += 8100;
                        break;
                    default:
                        salarySum = 0;
                        break;
                }
            }
            label_Copy.Content = salarySum.ToString()+",00";
        }

        private void updateProductButton_Click(object sender, RoutedEventArgs e)
        {
            //useless shit needed only for successful compilation
        }

        private void deleteProductButton_Click(object sender, RoutedEventArgs e)
        {
            //useless shit needed only for successful compilation
        }

        private void logOutBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserInfo.worker_id = -1;
                UserInfo.worker_name = "";
                UserInfo.worker_email = "";
                UserInfo.worker_position = "";

                LoginScreen loginScreen = new LoginScreen();
                loginScreen.Show();
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void showDeliveriesDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.Visibility == Visibility.Hidden)
                {
                    dataGrid.Visibility = Visibility.Visible;
                    dataGrid.ItemsSource = kursovaData.Deliveries.Local.ToBindingList();
                }
                else
                {
                    dataGrid.Visibility = Visibility.Hidden;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }
    }
}
