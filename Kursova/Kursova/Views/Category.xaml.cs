﻿using Kursova.Models;
using System;
using System.Data.Entity;
using System.Windows;
using System.Windows.Controls;

namespace Kursova.Views
{
    /// <summary>
    /// Interaction logic for Category.xaml
    /// </summary>
    public partial class Category : Window
    {
        KursovaEntities kursovaData;
        DirectorScreen director;
        public Category()
        {
            InitializeComponent();
            kursovaData = new KursovaEntities();
            kursovaData.Categories.Load();
            dataGrid.ItemsSource = kursovaData.Categories.Local.ToBindingList();
            this.Closing += MainWindow_Closing;
        }
        public Category(DirectorScreen dir)
        {
            InitializeComponent();

            this.director = dir;
            dir.IsEnabled = false;

            kursovaData = new KursovaEntities();
            kursovaData.Categories.Load();
            dataGrid.ItemsSource = kursovaData.Categories.Local.ToBindingList();
            this.Closing += MainWindowParam_Closing;
        }

        private void MainWindowParam_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.director.IsEnabled = true;
            kursovaData.Dispose();
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            kursovaData.Dispose();
        }

        private void showAdressDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.Visibility == Visibility.Hidden)
                {
                    dataGrid.Visibility = Visibility.Visible;
                    dataGrid.ItemsSource = kursovaData.Categories.Local.ToBindingList();
                }
                else
                {
                    dataGrid.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void addAdressBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Categories category = new Categories();
                category.Category_Name = categoryNameTextBox.Text.Trim();
                category.Category_Description = categoryDescriptionTextBox.Text.Trim();

                kursovaData.Categories.Add(category);
                kursovaData.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void updateAdressButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.SelectedItem != null)
                {
                    Categories updatedCategory = (Categories)dataGrid.SelectedItem;
                    updatedCategory.Category_Name = categoryNameTextBox.Text.Trim();
                    updatedCategory.Category_Description = categoryDescriptionTextBox.Text.Trim();

                    dataGrid.Items.Refresh();

                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to update.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void deleteAdressButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Categories category = dataGrid.SelectedItem as Categories;
                if (category != null)
                {
                    kursovaData.Categories.Remove(category);
                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to delete.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void dataGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            Models.Categories row_selected = dg.SelectedItem as Models.Categories;
            if (row_selected != null)
            {
                categoryNameTextBox.Text = row_selected.Category_Name;
                categoryDescriptionTextBox.Text = row_selected.Category_Description;
            }
        }
    }
}
