﻿using Kursova.Models;
using System;
using System.Data.SqlClient;
using System.Windows;

namespace Kursova.Views
{
    /// <summary>
    /// Interaction logic for LoginScreen.xaml
    /// </summary>
    public partial class LoginScreen : Window
    {
        KursovaEntities kursovaData;
        static String connectionString = "data source=DESKTOP-U9KRADE\\SQLEXPRESS;initial catalog=Kursova;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework";
        SqlConnection con;
        SqlDataAdapter dataAdapter = new SqlDataAdapter();
        SqlCommand cmd;
        SqlDataReader reader;
        //static String connectionString = "Server=.;Database=Kursova;Trusted_Connection=True;";
        //SqlConnection connection = new SqlConnection("Server=.;Database=Kursova;Trusted_Connection=True;");
        public LoginScreen()
        {
            InitializeComponent();
            //kursovaData = new KursovaEntities();
            //kursovaData.Workers.Load();
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            String message = "Invalid Credentials";
            try
            {
                con = new SqlConnection(connectionString);
                con.Open();
                cmd = new SqlCommand("Select * from Workers where worker_email=@worker_email", con);
                cmd.Parameters.AddWithValue("@worker_email", txtUsername.Text.ToString());
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["worker_pass"].ToString().Equals(txtPassword.Password.ToString(), StringComparison.InvariantCulture))
                    {
                        message = "1";
                        UserInfo.worker_email = txtUsername.Text.ToString();
                        UserInfo.worker_name = reader["worker_name"].ToString();
                        UserInfo.worker_position = reader["Position_ID"].ToString();
                        UserInfo.worker_id = (int)reader["Worker_ID"];
                    }
                }
                reader.Close();
                reader.Dispose();
                cmd.Dispose();
                con.Close();
            }
            catch (Exception ex)
            {
                message = ex.Message.ToString();
            }
            try
            {
                if (message == "1" && UserInfo.worker_position == "1")
                {
                    ManagerScreen managerScreen = new ManagerScreen();
                    managerScreen.Show();
                    this.Close();
                }
                else 
                {
                    if (message == "1" && UserInfo.worker_position == "3")
                    {
                        StorageWorkerScreen storageWorkerScreen = new StorageWorkerScreen();
                        storageWorkerScreen.Show();
                        this.Close();
                    }
                    else
                    {
                        if (message == "1" && UserInfo.worker_position == "4")
                        {
                            AdministratorScreen administratorScreen = new AdministratorScreen();
                            administratorScreen.Show();
                            this.Close();
                        }
                        else
                        {
                            if (message == "1" && UserInfo.worker_position == "5")
                            {
                                AccountantScreen accountantScreen = new AccountantScreen();
                                accountantScreen.Show();
                                this.Close();
                            }
                            else
                            {
                                if (message == "1" && UserInfo.worker_position == "6")
                                {
                                    DirectorScreen directorScreen = new DirectorScreen();
                                    directorScreen.Show();
                                    this.Close();
                                }
                                else
                                {
                                    MessageBox.Show(message, "Info");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }
    }
}
