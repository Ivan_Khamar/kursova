﻿using Kursova.Models;
using Kursova.Views;
using System;
using System.Data.Entity;
using System.Windows;
using System.Windows.Controls;

namespace Kursova
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ManagerScreen manScreen;
        KursovaEntities kursovaData;
        public MainWindow()
        {
            InitializeComponent();

            kursovaData = new KursovaEntities();
            kursovaData.Products.Load();
            dataGrid.ItemsSource = kursovaData.Products.Local.ToBindingList();
            kursovaData.Order_Products.Load();
            dataGrid_OrderProducts.ItemsSource = kursovaData.Order_Products.Local.ToBindingList();

            this.Closing += MainWindow_Closing;
        }

        public MainWindow(ManagerScreen managerScreen)
        {
            InitializeComponent();

            this.manScreen = managerScreen;
            managerScreen.IsEnabled = false;

            kursovaData = new KursovaEntities();
            kursovaData.Products.Load();
            dataGrid.ItemsSource = kursovaData.Products.Local.ToBindingList();
            kursovaData.Order_Products.Load();
            dataGrid_OrderProducts.ItemsSource = kursovaData.Order_Products.Local.ToBindingList();

            this.Closing += MainWindowParam_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            kursovaData.Dispose();
        }


        private void MainWindowParam_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.manScreen.IsEnabled = true;
            kursovaData.Order_Products.Load();
            this.manScreen.dataGrid.ItemsSource = kursovaData.Order_Products.Local.ToBindingList();
            this.manScreen.dataGrid.Items.Refresh();
        }

        private void showOrderProductsDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid_OrderProducts.Visibility == Visibility.Hidden)
                {
                    dataGrid_OrderProducts.Visibility = Visibility.Visible;
                    dataGrid_OrderProducts.ItemsSource = kursovaData.Order_Products.Local.ToBindingList();
                }
                else
                {
                    dataGrid_OrderProducts.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void addProductBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Order_Products orderProduct = new Order_Products();
                orderProduct.Order_ID = Int32.Parse(orderIDTextBox.Text.Trim());
                orderProduct.Product_ID = Int32.Parse(productIDTextBox.Text.Trim());

                kursovaData.Order_Products.Add(orderProduct);
                kursovaData.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void updateProductButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid_OrderProducts.SelectedItem != null)
                {
                    Order_Products updatedOrderProduct = (Order_Products)dataGrid_OrderProducts.SelectedItem;
                    updatedOrderProduct.Order_ID = Int32.Parse(orderIDTextBox.Text.Trim());
                    updatedOrderProduct.Product_ID = Int32.Parse(productIDTextBox.Text.Trim());

                    dataGrid_OrderProducts.Items.Refresh();

                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to update.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void deleteProductButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Order_Products orderProduct = dataGrid_OrderProducts.SelectedItem as Order_Products;
                if (orderProduct != null)
                {
                    kursovaData.Order_Products.Remove(orderProduct);
                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to delete.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void showProductsDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.Visibility == Visibility.Hidden)
                {
                    dataGrid.Visibility = Visibility.Visible;
                    dataGrid.ItemsSource = kursovaData.Products.Local.ToBindingList();
                }
                else
                {
                    dataGrid.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void logOutBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserInfo.worker_id = -1;
                UserInfo.worker_name = "";
                UserInfo.worker_email = "";
                UserInfo.worker_position = "";

                LoginScreen loginScreen = new LoginScreen();
                loginScreen.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void dataGrid_OrderProducts_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            Models.Order_Products row_selected = dg.SelectedItem as Models.Order_Products;
            if (row_selected != null)
            {
                orderIDTextBox.Text = row_selected.Order_ID.ToString();
                productIDTextBox.Text = row_selected.Product_ID.ToString();
            }
        }
    }
}
