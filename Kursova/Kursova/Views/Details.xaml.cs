﻿using Kursova.Models;
using System;
using System.Data.Entity;
using System.Windows;
using System.Windows.Controls;

namespace Kursova.Views
{
    /// <summary>
    /// Interaction logic for Details.xaml
    /// </summary>
    public partial class Details : Window
    {
        KursovaEntities kursovaData;
        AdministratorScreen admScreen;
        public Details()
        {
            InitializeComponent();
            kursovaData = new KursovaEntities();
            kursovaData.Delivery_Details.Load();
            dataGrid.ItemsSource = kursovaData.Delivery_Details.Local.ToBindingList();
            kursovaData.Order_Products.Load();
            dataGrid_OrderProducts.ItemsSource = kursovaData.Order_Products.Local.ToBindingList();

            this.Closing += MainWindow_Closing;
        }

        public Details(AdministratorScreen adminScreen)
        {
            InitializeComponent();

            this.admScreen = adminScreen;
            adminScreen.IsEnabled = false;

            kursovaData = new KursovaEntities();
            kursovaData.Delivery_Details.Load();
            dataGrid.ItemsSource = kursovaData.Delivery_Details.Local.ToBindingList();
            kursovaData.Order_Products.Load();
            dataGrid_OrderProducts.ItemsSource = kursovaData.Order_Products.Local.ToBindingList();

            this.Closing += MainWindowParam_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            kursovaData.Dispose();
        }
        private void MainWindowParam_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.admScreen.IsEnabled = true;
            kursovaData.Delivery_Details.Load();
            this.admScreen.dataGrid_Details.ItemsSource = kursovaData.Delivery_Details.Local.ToBindingList();
            this.admScreen.dataGrid_Details.Items.Refresh();
            //kursovaData.Dispose();
        }

        private void showDeliveryDetailsDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.Visibility == Visibility.Hidden)
                {
                    dataGrid.Visibility = Visibility.Visible;
                    dataGrid.ItemsSource = kursovaData.Delivery_Details.Local.ToBindingList();
                }
                else
                {
                    dataGrid.Visibility = Visibility.Hidden;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void addDetailBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Delivery_Details deliveryDetails = new Delivery_Details();
                deliveryDetails.Product_Quantity = Int32.Parse(productQuantityTextBox.Text.Trim());
                deliveryDetails.Unit_cost = Int32.Parse(unitCostTextBox.Text.Trim());
                deliveryDetails.Order_Product_ID = Int32.Parse(orderProductIDTextBox.Text.Trim());

                kursovaData.Delivery_Details.Add(deliveryDetails);
                kursovaData.SaveChanges();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void updateDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid.SelectedItem != null)
                {
                    Delivery_Details updatedDeliveryDetails = (Delivery_Details)dataGrid.SelectedItem;
                    updatedDeliveryDetails.Product_Quantity = Int32.Parse(productQuantityTextBox.Text.Trim());
                    updatedDeliveryDetails.Unit_cost = Int32.Parse(unitCostTextBox.Text.Trim());
                    updatedDeliveryDetails.Order_Product_ID = Int32.Parse(orderProductIDTextBox.Text.Trim());

                    dataGrid.Items.Refresh();

                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to update.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void deleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Delivery_Details deliveryDetail = dataGrid.SelectedItem as Delivery_Details;
                if (deliveryDetail != null)
                {
                    kursovaData.Delivery_Details.Remove(deliveryDetail);
                    kursovaData.SaveChanges();
                }
                else
                {
                    System.Windows.MessageBox.Show("Select row you want to delete.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void showOrderProductsBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGrid_OrderProducts.Visibility == Visibility.Hidden)
                {
                    dataGrid_OrderProducts.Visibility = Visibility.Visible;
                    dataGrid_OrderProducts.ItemsSource = kursovaData.Order_Products.Local.ToBindingList();
                }
                else
                {
                    dataGrid_OrderProducts.Visibility = Visibility.Hidden;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }

        private void logOutBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void dataGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            Models.Delivery_Details row_selected = dg.SelectedItem as Models.Delivery_Details;
            if (row_selected != null)
            {
                productQuantityTextBox.Text = row_selected.Product_Quantity.ToString();
                unitCostTextBox.Text = row_selected.Unit_cost.ToString();
                orderProductIDTextBox.Text = row_selected.Order_Product_ID.ToString();
            }
        }
    }
}
