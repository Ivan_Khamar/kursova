﻿using System;
using System.Windows;

namespace Kursova.Views
{
    /// <summary>
    /// Interaction logic for DirectorScreen.xaml
    /// </summary>
    public partial class DirectorScreen : Window
    {
        public DirectorScreen()
        {
            InitializeComponent();
        }

        private void showProductsDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Workers workers = new Workers(this);
                workers.Show();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
            
        }
        private void showCategoriesDataBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Positions positions = new Positions(this);
                positions.Show();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void showLocationButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Locations locations = new Locations(this);
                locations.Show();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void showAddressDataButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Adresses adress = new Adresses(this);
                adress.Show();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void showLocationTypesButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LocationType locType = new LocationType(this);
                locType.Show();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }
        private void logOutBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserInfo.worker_id = -1;
                UserInfo.worker_name = "";
                UserInfo.worker_email = "";
                UserInfo.worker_position = "";

                LoginScreen loginScreen = new LoginScreen();
                loginScreen.Show();
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }

        private void showCategoriesButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Category category = new Category(this);
                category.Show();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().ToString());
            }
        }
    }
}
